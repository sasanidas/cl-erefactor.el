;;; test-cl-erefactor.el --- cl-erefactor test file.               -*- lexical-binding: t; -*-

;; Copyright (C) 20 Fermin Munoz

;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;; The requires
(require 'ert)
(require 'cl-erefactor)

;;(load (expand-file-name "cl-erefactor.el" "../"))

(ert-deftest cl-erefactor-in-string-or-comment-t ()
  (should
   (let* ((test-file "example.lisp" ))
     (and
      (numberp (with-temp-buffer
		 (insert-file-contents test-file)
		 (goto-char 61)
		 (cl-erefactor--in-string-or-comment (point))))

      (equal nil   (with-temp-buffer
		     (insert-file-contents test-file)
		     (goto-char 73)
		     (cl-erefactor--in-string-or-comment (point))))))))

(ert-deftest cl-erefactor-function-position-t ()
  (should
   (let* ((test-file "example.lisp")
	  (function-header-position 122))
     (= function-header-position (with-temp-buffer
				   (lisp-mode)
				   (insert-file-contents test-file)
				   (goto-char 756)
				   (cl-erefactor--function-position (point)))))))


(ert-deftest cl-erefactor-function-tail-t ()
  (should
   (let* ((test-file "example.lisp")
	  (function-header-position 122)
	  (tail-position 2984))
     (= tail-position (with-temp-buffer
			(lisp-mode)
			(insert-file-contents "example.lisp")
			(goto-char 756)
			(cl-erefactor--function-tail
			 (cl-erefactor--function-position (point))))))))

(ert t)



(provide 'test-cl-erefactor.el)
;;; test-cl-erefactor.el ends here
