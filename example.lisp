(defun simple-test (sym)
"This is a comment, not related to sym"
(fancy(stuff)))


;; Random common lisp maxima function
(defun complex-test (aarray ind1 &rest inds)
  (declare (special fixunbound flounbound))
  (typecase aarray
    (cl:array
     (case (array-element-type aarray)
       ((flonum fixnum t)
	(apply #'aref aarray ind1 inds))
       (t
	(merror (intl:gettext "MARRAYREF: encountered array ~M of unknown type.") aarray))))
    (cl:hash-table
     (gethash (if inds (cons ind1 inds) inds) aarray))
    (cl:symbol
     (if $use_fast_arrays
         (let ((tem (and (boundp aarray) (symbol-value aarray))))
           (simplify (cond ((arrayp tem)
                            (apply #'aref tem ind1 inds))
                           ((hash-table-p tem)
                            (gethash (if inds (cons ind1 inds) inds) tem))
                           ((eq aarray 'mqapply)
                            (apply #'marrayref ind1 inds))
                           ((mget aarray 'hashar)
                            (harrfind `((,aarray array) ,ind1 ,@inds)))
                           ((symbolp tem)
                            `((,tem array) ,ind1 ,@inds))
                           (t
                            (error "unknown type of array for use_fast_arrays. ~
			       the value cell should have the array or hash table")))))
         (let (ap)                      ; no fast arrays
           (simplify (cond ((setq ap (get aarray 'array))
                            (let ((val (if (null inds)
                                           (aref ap ind1)
                                           (apply #'aref (append (list ap ind1) inds)))))
                              ;; Check for KLUDGING array function implementation.
                              (if (case (array-element-type ap)
                                    ((flonum) (= val flounbound))
                                    ((fixnum) (= val fixunbound))
                                    ((t) (eq val munbound))
                                    (t (merror (intl:gettext "MARRAYREF: encountered array pointer ~S of unknown type.") ap)))
                                  (arrfind `((,aarray ,aarray) ,ind1 ,@inds))
                                  val)))
                           ((setq ap (mget aarray 'array))
                            (arrfind `((,aarray array) ,ind1 ,@inds)))
                           ((setq ap (mget aarray 'hashar))
                            (harrfind `((,aarray array) ,ind1  ,@inds)))
                           ((eq aarray 'mqapply)
                            (apply #'marrayref ind1 inds))
                           (t
                            `((,aarray  array) ,ind1  ,@inds)))))))
    (cl:list
     (simplify (if (member (caar aarray) '(mlist $matrix) :test #'eq)
		   (list-ref aarray (cons ind1 inds))
		   `((mqapply aarray) ,aarray ,ind1 ,@inds))))
    (t
     (merror (intl:gettext "MARRAYREF: cannot retrieve an element of ~M") aarray))))
