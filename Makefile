EMACS=emacs
FILES=cl-erefactor.el

.PHONY: help package elpa clean make-test compile-test test lint

help:
	@printf "\
Main targets\n\
compile    -- compile .el files\n\
elpa 	   -- create a package with the elpa format \n\
package    -- create a tar.gz file with the .el files \n\
test       -- run tests in batch mode\n\
clean      -- delete generated files\n\
lint       -- run package-lint in batch mode\n\
help       -- print this message\n"

package: *.el
	@ver=`grep -o "Version: .*" cl-erefactor.el | cut -c 10-`; \
	tar czvf cl-erefactor-$$ver.tar.gz --mode 644 $$(find . -name \*.el)

elpa: *.el
	@version=`grep -o "Version: .*" cl-erefactor.el | cut -c 10-`; \
	dir=cl-erefactor-$$version; \
	mkdir -p "$$dir"; \
	cp $$(find . -name \*.el) cl-erefactor-$$version; \
	echo "(define-package \"cl-erefactor\" \"$$version\" \
	\"Modular in-buffer completion framework\")" \
	> "$$dir"/cl-erefactor-pkg.el; \
	tar cvf cl-erefactor-$$version.tar --mode 644 "$$dir"

clean:
	@rm -rf cl-erefactor-*/ cl-erefactor-*.tar cl-erefactor-*.tar.bz2 *.elc ert.el .elpa/

make-test:
	${EMACS}  --batch -l test/make-install.el -l test/make-test.el 

test: make-test clean

compile:
	${EMACS} --batch  -l test/make-install.el -L . -f batch-byte-compile cl-erefactor.el 

compile-test: compile clean

lint:
	${EMACS} --batch -l test/make-install.el -f package-lint-batch-and-exit ${FILES}
