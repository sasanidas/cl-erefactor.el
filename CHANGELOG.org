* Changelog
All notable changes to this project will be documented in this file.

The format is based on [[https://keepachangelog.com/en/1.0.0/][Keep a Changelog]].
