;;; cl-erefactor.el ---  Common lisp refactoring utilities            -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 13 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/cl-erefactor.el.git
;; Package-Requires: ((emacs "26.3"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires

(require 'cl-lib)
(require 'syntax)
(require 'imenu)
(require 'seq)


(defgroup cl-erefactor nil
  "cl-erefactor utilities group."
  :prefix "cl-erefactor-"
  :tag "Cl-erefactor"
  :group 'utilities
  :link '(url-link :tag "Repository" "https://gitlab.com/sasanidas/cl-erefactor.el.git"))

(defcustom cl-erefactor-semantic-integration nil
  "It enable the semantic framework integration.
Cl-erefactor has a simple parser that can understand
certain Common Lisp constructs, that uses the Emacs
semantic framework as a backend."
  :group 'cl-erefactor
  :type 'boolean)

(defun cl-erefactor--create-regexp (symbol)
  "Create SYMBOL exclusive regexp."
  (format "\\_<\\(%s\\)\\_>" (regexp-quote symbol)))

(defun cl-erefactor--in-string-or-comment (p)
  "Return non-nil if P is within a string or comment."
  (let ((ppss (syntax-ppss p)))
    (or (car (setq ppss (nthcdr 3 ppss)))
        (car (setq ppss (cdr ppss)))
        (nth 3 ppss))))

(defun cl-erefactor--rescan-imenu ()
  "Force imenu to rescan the current buffer."
  (setq imenu--index-alist nil)
  (imenu--make-index-alist))

(defun cl-erefactor--function-tail (function-head)
  "Return the function tail base on FUNCTION-HEAD point."
  (if (and cl-erefactor-semantic-integration semantic-mode)
      (semantic-tag-end (semantic-current-tag))
    (save-excursion
      (goto-char function-head)
      (scan-sexps (point) 1))))


(defun cl-erefactor--function-position (upoint)
  "Get the root function position of the closest defun.
Using the current point UPOINT."
  (if (and cl-erefactor-semantic-integration semantic-mode)
      (semantic-tag-start (semantic-current-tag))
    (let* ((entries (cl-erefactor--rescan-imenu))
	   (pos-entries (remove nil (mapcar (lambda (entry)
					      (when (markerp (cdr entry))
						(cdr entry)))
					    (cl-erefactor--rescan-imenu))))
	   (num-entries (mapcar (lambda (entry) (- upoint (marker-position entry)))
				pos-entries))
	   (fun-p (- (length (remove nil (seq-map (lambda (number) (>= number 0)) num-entries)))
		     1)))
      (marker-position (nth fun-p pos-entries)))))



;;TODO: Make a test for this function
(defun cl-erefactor-rename-region (symb new-symbol beg end)
  "Search in the region from BEG to END the references of SYMB.
Changing the match with NEW-SYMBOL."
  (let* ((regexp (cl-erefactor--create-regexp symb)))
    (save-excursion
      (goto-char beg)
      (while (and (re-search-forward regexp nil t)
		  (< (point) end))
	(let ((target (match-data)))
	  (when (not (cl-erefactor--in-string-or-comment (point)))
	    (goto-char (match-end 1))
	    (unwind-protect
		(when (y-or-n-p "Rename? ")
		  (set-match-data target)
		  (replace-match new-symbol t nil nil 1)))))))))



(defun cl-erefactor-replace-symbol-region (old-symbol new-symbol)
  "Replace in active region OLD-SYMBOL with NEW-SYMBOL."
  (interactive "sSymbol to replace: \nsNew symbol: ")
  (let* ((r-begin (region-beginning))
	 (r-end (region-end)))
    (cl-erefactor-rename-region old-symbol new-symbol r-begin r-end)))


(defun cl-erefactor-replace-symbol-defun (new-symbol)
  "Replace the symbol at `point' with NEW-SYMBOL."
  (interactive "sReplace with: ")
  (let* ((current-symbol (thing-at-point 'symbol t))
	 (function-header (cl-erefactor--function-position (point)))
	 (function-tail (cl-erefactor--function-tail function-header)))
    (cl-erefactor-rename-region current-symbol new-symbol function-header function-tail)))


(provide 'cl-erefactor)
;;; cl-erefactor.el ends here
